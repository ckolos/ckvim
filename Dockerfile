FROM alpine:latest
RUN apk update \
      && apk upgrade \
      && apk add bash editorconfig git vim
WORKDIR /root
RUN git clone https://github.com/ckolos/vim-setup
RUN /root/vim-setup/vimsetup.sh
COPY script .
RUN vim -s script
CMD ["vim"]
